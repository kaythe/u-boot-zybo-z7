################################################################################
#
# Vagrantfile
#
################################################################################

# U-Boot version to use
RELEASE='2019.10-rc3'

### Change here for more memory/cores ###
VM_MEMORY=4096
VM_CORES=2

Vagrant.configure('2') do |config|
	config.vm.box = 'ubuntu/bionic64'

	config.vm.provider :vmware_fusion do |v, override|
		v.vmx['memsize'] = VM_MEMORY
		v.vmx['numvcpus'] = VM_CORES
	end

	config.vm.provider :virtualbox do |v, override|
		v.memory = VM_MEMORY
		v.cpus = VM_CORES

		required_plugins = %w( vagrant-vbguest )
		required_plugins.each do |plugin|
		  system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
		end
	end

	config.vm.provision 'shell' do |s|
		s.inline = 'echo Setting up machine name'

		config.vm.provider :vmware_fusion do |v, override|
			v.vmx['displayname'] = "U-Boot #{RELEASE}"
		end

		config.vm.provider :virtualbox do |v, override|
			v.name = "U-Boot #{RELEASE}"
		end
	end

	config.vm.provision 'shell', privileged: true, inline:
		"sed -i 's|deb http://us.archive.ubuntu.com/ubuntu/|deb mirror://mirrors.ubuntu.com/mirrors.txt|g' /etc/apt/sources.list
		apt-get -q update
		apt-get purge -q -y snapd lxcfs lxd ubuntu-core-launcher snap-confine
		apt-get -q -y install build-essential libncurses5-dev \
			git bzr unzip bc bison flex libssl-dev
		apt-get -q -y autoremove
		apt-get -q -y clean
		update-locale LC_ALL=C"

	config.vm.provision 'shell', privileged: false, inline:
		"echo 'Downloading and extracting U-Boot #{RELEASE}'
		wget -q -c https://github.com/u-boot/u-boot/archive/v#{RELEASE}.tar.gz
		tar axf v#{RELEASE}.tar.gz"

	config.vm.provision 'shell', privileged: false, inline:
		"echo 'Downloading and extracting Linaro toolchain'
		wget -q -c https://releases.linaro.org/components/toolchain/binaries/latest-7/arm-linux-gnueabihf/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf.tar.xz
		tar axf gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf.tar.xz
		echo 'export PATH=$HOME/gcc-linaro-7.4.1-2019.02-x86_64_arm-linux-gnueabihf/bin:$PATH' >> .bashrc
		echo 'export ARCH=arm' >> .bashrc
		echo 'export CROSS_COMPILE=arm-linux-gnueabihf-' >> .bashrc"
end
